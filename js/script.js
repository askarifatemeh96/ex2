//global number for counting root node and regular node
nodeId = 1;
rootId = 1;


function createRootNode() {
    document.body.innerHTML += `<div id="root${rootId}">
    <p id="pRoot${rootId}">Root Node ${rootId} </p>
    <button class="btn" onclick="createNode('root${rootId}')">create</button>
    <button class="btn" onclick="removeNode('root${rootId}')">remove</i></button>
    <button class="btn" onclick="editNode('pRoot${rootId}')">edit</button>
</div>`
    rootId++;
}

function createNode(parentId) {
    console.log(parentId);
    var node = document.createElement("div");
    node.id = nodeId;
    node.innerHTML = `
    <p id="p${nodeId}">Node ${nodeId}</p>
    <button class="btn" onclick=createNode(this.parentElement.id)>create</button>
    <button class="btn" onclick=removeNode(this.parentElement.id)>remove</button>
    <button class="btn" onclick="editNode('p${nodeId}')">edit</button>
    `
    document.getElementById(parentId).appendChild(node);
    node.style.backgroundColor = `rgba(1,1,1,0.2)`
    nodeId++;

}

function removeNode(parentId) {
    document.getElementById(parentId).remove();
}

function editNode(pId) {
    document.getElementById(pId).innerHTML = prompt("Edit name :) ");
}

var numberOfRoot = window.prompt("Enter number of root element :) ");
while (numberOfRoot > 0) {
    createRootNode();
    numberOfRoot--;
}

